#include <iostream>
#include <Windows.h>

void setWindowWallpaper(const char* path){
    SystemParametersInfo(SPI_SETDESKWALLPAPER, 0, (PVOID)path, SPIF_UPDATEINIFILE | SPIF_SENDWININICHANGE);
}

std::string getTempDir(){
    char tempDir[MAX_PATH];
    GetTempPath(MAX_PATH, tempDir);
    return tempDir;
}

void downloadWallpaper(const char* url, const char* path){
    std::string command = "powershell -Command \"(New-Object System.Net.WebClient).DownloadFile('" + std::string(url) + "', '" + std::string(path) + "')\"";
    system(command.c_str());
}

int main(){
    const char* urls[] = {
        "https://i.imgur.com/PaxD7Cd.png",
        "https://i.imgur.com/NE2UzPX.png",
        "https://i.imgur.com/ebOl5nd.png",
        "https://i.imgur.com/AMYNPGT.png"
    };

    for(int i = 0; i < 4; i++){
        downloadWallpaper(urls[i], (getTempDir() + std::to_string(i) + ".png").c_str());
    }

    while(true){
        setWindowWallpaper((getTempDir() + std::to_string(rand() % 4) + ".png").c_str());
        Sleep(1000);
    }

    return 0;
}